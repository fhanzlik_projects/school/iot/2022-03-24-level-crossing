Canon canon(Motor::Pins { dir: 12, pwm: 3 });
Semaphore semaphore(750, Semaphore::Pins { white: 5, red_0: 2, red_1: 4 });
Button fireButton(20);
DistanceSensor distanceSensor(DistanceSensor::Pins { ping: 6, echo: 7 });
Timer distanceReadTimer(100);

void fireButtonPressed() { fireButton.markPressed(); }

void setup() {
	Serial.begin(115200);

	canon.setup();
	fireButton.setup(fireButtonPressed);
	distanceSensor.setup();

	while (!Serial) { delay(10); }
}

instant_t lastLoopTime = 0;
void loop() {
	const instant_t now = millis();
	const duration_t delta = lastLoopTime == 0 ? 0 : now - lastLoopTime;
	lastLoopTime = now;
	
	// control over serial port
	switch (Serial.read()) {
		case 'f':
			Serial.println("firing.");
			canon.fire();
			break;
		case 'n':
			Serial.println("changing semaphore state to normal.");
			semaphore.set_state(Semaphore::State::normal);
			break;
		case 'c':
			Serial.println("changing semaphore state to closed.");
			semaphore.set_state(Semaphore::State::closed);
			break;
	}

	if (fireButton.pressed()) {
		canon.fire();
	}

	if (distanceReadTimer.tick(delta)) {
		semaphore.set_state(distanceSensor.readCm() < 12 ? Semaphore::State::normal : Semaphore::State::closed);
	}

	semaphore.tick(delta);
	canon.tick(delta);

	delay(10);
}

