/// async timer for periodically running specific tasks
class Timer {
	private:
		instant_t time = 0;
		duration_t length;

	public:
		explicit Timer(duration_t length) : length(length) {}

		/// resets the timer to start timing again
		void reset() {
			this->time = 0;
		}

		/// adds the specified delta to the timer and return whether it has fired during this period
		bool tick(duration_t delta) {
			this->time += delta;
			if (this->time > this->length) {
				this->time = this->time % this->length;
				return true;
			}
			return false;
		}
};

/// async timer that only fires once after each activation
class Countdown {
	private:
		bool active = false;
		instant_t time = 0;
		duration_t length;

	public:
		explicit Countdown(duration_t length) : length(length) {}

		/// start the countdown if it wasn't active or restart it if it was.
		void start() {
			this->active = true;
			this->time = 0;
		}

		/// adds the specified delta to the timer and return whether it has fired during this period
		bool tick(duration_t delta) {
			if (!this->active) return false;
				
			this->time += delta;

			if (this->time > this->length) {
				this->active = false;

				return true;
			}
			return false;
		}
};
